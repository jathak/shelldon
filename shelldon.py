import os
import wit
import json
import rumps
import time
import re
import subprocess
import sys
import pyaudio
from pocketsphinx import *

class ShelldonApp(rumps.App):

	def __init__(self):
		super(ShelldonApp, self).__init__("~", quit_button=None)
		self.menu = [rumps.MenuItem("Listen",key="l"), rumps.MenuItem("Go to",key="g"), rumps.MenuItem("Edit Settings"), rumps.MenuItem("Reload Settings"), rumps.MenuItem("Quit",key="q")]

	@rumps.clicked("Listen")
	def listen_item(self, _):
		listen()

	@rumps.clicked("Edit Settings")
	def edit_settings(self, _):
		intent_fn("shelldon_config",None)()

	@rumps.clicked("Reload Settings")
	def reload_settings(self, _):
		intent_fn("reload_json",None)()

	@rumps.clicked("Go to")
	def go_to(self, _):
		window = rumps.Window("Enter directory", "Go to", app.title, None, True)
		response = window.run()
		if response.clicked == 1:
			directory = get_new_directory(response.text)
			if directory!=None:
				app.title = directory

	@rumps.clicked("Quit")
	def quit(self, _):
		global aliases, app_running
		if os.path.isfile('shelldon.json'):
			prefs = json.load(open('shelldon.json'))
			aliases = prefs['aliases']
		new_prefs = {"currentDirectory": app.title, "aliases":aliases}
		with open('shelldon.json', 'w') as outfile:
			json.dump(new_prefs, outfile, indent=4)
		app_running = False
		rumps.quit_application()

aliases = []

def min_confidence_level(intent):
	if intent == "cd":
		return 0.6
	if intent == "ls":
		return 0.5
	return 0.4

def intent_fn(intent, entities):
	fn = None
	if intent == "cd":
		def fn():
			directory = get_new_directory(entities['directory'][0]['value'])
			if directory == None:
				return
			app.title = directory

	if intent == "ls":
		def fn():
			directory = app.title
			if directory[0] == "~":
				directory = os.path.expanduser('~') + "/"+ directory[1:]
			dirs = os.listdir(directory)
			ls_str = ""
			for item in dirs:
				ls_str += item + "\n"
			rumps.alert("Files in "+app.title, ls_str)

	if intent == "mkdir":
		def fn():
			directory = entities['directory'][0]['value']
			run_command("mkdir "+directory)
			announce("Created new directory, "+directory, True, False)

	if intent == "git_push":
		def fn():
			message = ""
			if entities == None or len(entities) == 0:
				response = rumps.Window("Enter a message for this commit", "Git", '', None, True).run()
				if response.clicked == 1:
					message = response.text
				else:
					return
			else:
				message = entities['message'][0]['value']
			run_command("git add . && git commit -m '%s' && git push origin master"%message)
			announce("Git changes pushed",True, False)

	if intent == "git_pull":
		def fn():
			run_command("git pull")
			announce("Git changes pulled",True, False)

	if intent == "open_app":
		def fn():
			dirs = os.listdir("/Applications")
			dirs += os.listdir("/Applications/Utilities")
			application = entities['application'][0]['value'].lower()
			for item in dirs:
				app = item.lower()
				if application in app:
					if item == "Terminal.app":
						run_command("", True, True)
					else:
						run_command("cd /Applications && open '%s'"%app)

	if intent == "shelldon_config":
		def fn():
			global aliases
			if os.path.isfile('shelldon.json'):
				prefs = json.load(open('shelldon.json'))
				aliases = prefs['aliases']
			new_prefs = {"currentDirectory": app.title, "aliases":aliases}
			with open('shelldon.json', 'w') as outfile:
				json.dump(new_prefs, outfile, indent=4)
			os.system("open shelldon.json")

	if intent == "reload_json":
		def fn():
			global aliases
			if os.path.isfile('shelldon.json'):
				prefs = json.load(open('shelldon.json'))
				app.title = prefs['currentDirectory']
			if app.title == None or app.title == "":
				app.title = "~"
			print(app.title)
			aliases = prefs['aliases']
			announce("Settings reloaded", True, True)

	return fn

def get_new_directory(entity):
	entity = entity.lower()
	if entity == "home":
		entity = "~"
	if entity == "..":
		working_dir = app.title
		slashparts = working_dir.split('/')
		new_str = ""
		for part in slashparts[:-1]:
			new_str += part + "/"
		entity = new_str[:-1]
	elif entity[0] != "/" and entity[0] != "~":
		possibilities = [entity, entity.replace(" ", "_"), entity.replace(" ","")]
		if entity[-1]== 's':
			singular = entity[:-1]
			possibilities += [singular, singular.replace(" ","_"), singular.replace(" ","")]
		else:
			plural = entity + 's'
			possibilities += [plural, plural.replace(" ","_"), plural.replace(" ","")]
		final_entity = None
		for option in possibilities:
			full_option = app.title + "/"+ option
			full_option_test = full_option.replace("~",os.path.expanduser('~'))
			if os.path.isdir(full_option_test):
				final_entity = full_option
				break
		if final_entity == None:
			announce(entity+" is not a valid directory",True)
			return None
		entity = final_entity

	return entity


access_token = 'PCNEJV5VKA26FBAFG5KHE2WYIPIWGXVW'

listen_counter = 0

def listen():
	global listen_counter
	wit.init()
	wit.voice_query_auto_async(access_token, listener)
	beep()
	current_count = listen_counter
	time.sleep(10)
	if listen_counter == current_count:
		wit.voice_query_stop_async(listener)
		beep3()

def beep():
	run_command("afplay /System/Library/Sounds/Glass.aiff")

def beep2():
	run_command("afplay /System/Library/Sounds/Ping.aiff")

def beep3():
	run_command("afplay /System/Library/Sounds/Funk.aiff")

def listener(response):
	global running_command, listen_counter
	listen_counter += 1
	wit.close()
	running_command = False
	t = Thread(target=loop_keyword_spotting)
	t.start()
	resp = json.loads(response)
	outcomes = resp['outcomes']
	if len(outcomes) > 0:
		outcome = outcomes[0]
		text = outcome['_text']
		if "cancel cancel" in text or "cancel" == text:
			announce("Command canceled")
			return
		for alias in aliases:
			if re.match(alias['phrase'], text.lower()):
				run_command(alias['command'], alias['output'], True)
				beep2()
				return
		entities = outcome['entities']
		intent = outcome['intent']
		confidence = outcome['confidence']
		fn = intent_fn(intent, entities)
		if fn != None and confidence>=min_confidence_level(intent):
			beep2()
			fn()
		else:
			announce("Sorry, I didn't get that.")
			announce("User Input: "+text,True,False)

def run_command(command, use_terminal=False, macro=False):
	full_command = "cd "+app.title+" && "+command
	if command == "":
		full_command = full_command[:-3]
	if macro:
		if use_terminal:
			subprocess.Popen("""osascript -e 'tell app "Terminal" to do script "%s"'"""%full_command, shell=True)
		else:
			subprocess.Popen(full_command, shell=True)
	else:
		output = os.system(full_command)

def announce(text, notification=False, speaking=True):
	if speaking:
		say(text)
	if notification:
		rumps.notification("Shelldon",text,"", sound=False)

def say(text):
	os.system('say "%s"'%text)

app = ShelldonApp()
    
if os.path.isfile('shelldon.json'):
	prefs = json.load(open('shelldon.json'))
	app.title = prefs['currentDirectory']
	if app.title == None or app.title == "":
		app.title = "~"
	print(app.title)
	aliases = prefs['aliases']

app_running = True
running_command = False

modeldir = "/usr/local/share/pocketsphinx/model"
# Create a decoder with certain model
config = Decoder.default_config()
config.set_string('-hmm', os.path.join(modeldir, 'hmm/en_US/hub4wsj_sc_8k'))
config.set_string('-dict', os.path.join(modeldir, 'lm/en_US/cmu07a.dic'))
config.set_string('-keyphrase', 'ok sheldon')
config.set_float('-kws_threshold', 1e-40)



def loop_keyword_spotting():
	global running_command
	decoder = Decoder(config)
	decoder.start_utt('spotting')
	p = pyaudio.PyAudio()
	stream = p.open(format=pyaudio.paInt16, channels=1, rate=16000, input=True, frames_per_buffer=1024)
	stream.start_stream()   
	while app_running and not running_command:
		buf = stream.read(1024)
		decoder.process_raw(buf, False, False)
		if decoder.hyp() != None and decoder.hyp().hypstr == 'ok sheldon' and not running_command:
			print("Detected keyword")
			running_command = True
			decoder.end_utt()
			listen()  

from threading import Thread

t = Thread(target=loop_keyword_spotting)
t.start()

if __name__ == "__main__":
	app.run()

