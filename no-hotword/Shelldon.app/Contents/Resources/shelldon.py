import os
import wit
import json
import rumps
import time
import re
import subprocess

class ShelldonApp(rumps.App):

	def __init__(self):
		super(ShelldonApp, self).__init__("~", quit_button=None)
		self.menu = [rumps.MenuItem("Listen",key="l"), rumps.MenuItem("Go to",key="g"), rumps.MenuItem("Quit",key="q")]
	
	@rumps.clicked("Listen")
	def listen_item(self, _):
		listen()

	@rumps.clicked("Go to")
	def go_to(self, _):
		window = rumps.Window("Enter directory", "Go to", app.title, None, True)
		response = window.run()
		if response.clicked == 1:
			directory = get_new_directory(response.text)
			if directory!=None:
				app.title = directory

	@rumps.clicked("Quit")
	def quit(self, _):
		if os.path.isfile('shelldon.json'):
			prefs = json.load(open('shelldon.json'))
			aliases = prefs['aliases']
		new_prefs = {"currentDirectory": app.title, "aliases":aliases}
		with open('shelldon.json', 'w') as outfile:
			json.dump(new_prefs, outfile, indent=4)

		wit.close()
		rumps.quit_application()

aliases = []

def min_confidence_level(intent):
	if intent == "cd":
		return 0.6
	if intent == "ls":
		return 0.5
	return 0.4

def intent_fn(intent, entities):
	fn = None
	if intent == "cd":
		def fn():
			directory = get_new_directory(entities['directory'][0]['value'])
			if directory == None:
				return
			app.title = directory

	if intent == "ls":
		def fn():
			directory = app.title
			if directory[0] == "~":
				directory = os.path.expanduser('~') + "/"+ directory[1:]
			dirs = os.listdir(directory)
			ls_str = ""
			for item in dirs:
				ls_str += item + "\n"
			rumps.alert("Files in "+app.title, ls_str)

	if intent == "mkdir":
		def fn():
			directory = entities['directory'][0]['value']
			run_command("mkdir "+directory)
			announce("Created new directory, "+directory, True, False)

	if intent == "git_push":
		def fn():
			message = ""
			if entities == None or len(entities) == 0:
				response = rumps.Window("Enter a message for this commit", "Git", '', None, True).run()
				if response.clicked == 1:
					message = response.text
				else:
					return
			else:
				message = entities['message'][0]['value']
			run_command("git add . && git commit -m '%s' && git push origin master"%message)
			announce("Git changes pushed",True, False)

	if intent == "git_pull":
		def fn():
			run_command("git pull")
			announce("Git changes pulled",True, False)

	if intent == "open_app":
		def fn():
			dirs = os.listdir("/Applications")
			dirs += os.listdir("/Applications/Utilities")
			application = entities['application'][0]['value'].lower()
			for item in dirs:
				app = item.lower()
				if application in app:
					if item == "Terminal.app":
						run_command("", True, True)
					else:
						run_command("cd /Applications && open '%s'"%app)

	if intent == "shelldon_config":
		def fn():
			os.system("open shelldon.json")

	return fn

def get_new_directory(entity):
	if entity == "..":
		working_dir = app.title
		slashparts = working_dir.split('/')
		new_str = ""
		for part in slashparts[:-1]:
			new_str += part + "/"
		entity = new_str[:-1]
	elif entity[0] != "/" and entity[0] != "~":
		underscored = entity.replace(" ", "_")

		possibilities = [entity, underscored, entity.replace(" ","")]
		final_entity = None
		for option in possibilities:
			full_option = app.title + "/"+ option
			full_option_test = full_option.replace("~",os.path.expanduser('~'))
			if os.path.isdir(full_option_test):
				final_entity = full_option
				break
		if final_entity == None:
			announce("Invalid directory")
			announce(entity+" is not a valid directory",True, False)
			return None
		entity = final_entity

	return entity


access_token = 'PCNEJV5VKA26FBAFG5KHE2WYIPIWGXVW'

def listen():
	wit.voice_query_auto_async(access_token, listener)
	time.sleep(1)
	beep()

def beep():
	run_command("afplay /System/Library/Sounds/Glass.aiff")

def beep2():
	run_command("afplay /System/Library/Sounds/Ping.aiff")

def listener(response):
	resp = json.loads(response)
	outcomes = resp['outcomes']
	if len(outcomes) > 0:
		outcome = outcomes[0]
		text = outcome['_text']
		if "cancel cancel" in text or "cancel" == text:
			announce("Command canceled")
			return
		for alias in aliases:
			if re.match(alias['phrase'], text.lower()):
				run_command(alias['command'], alias['output'], True)
				beep2()
				return
		entities = outcome['entities']
		intent = outcome['intent']
		confidence = outcome['confidence']
		fn = intent_fn(intent, entities)
		if fn != None and confidence>=min_confidence_level(intent):
			fn()
			beep2()
		else:
			announce("Sorry, I didn't get that.")

def run_command(command, use_terminal=False, macro=False):
	full_command = "cd "+app.title+" && "+command
	if command == "":
		full_command = full_command[:-3]
	if macro:
		if use_terminal:
			subprocess.Popen("""osascript -e 'tell app "Terminal" to do script "%s"'"""%full_command, shell=True)
		else:
			subprocess.Popen(full_command, shell=True)
	else:
		output = os.system(full_command)

def announce(text, notification=False, speaking=True):
	if speaking:
		say(text)
	if notification:
		rumps.notification("Shelldon",text,"", sound=False)

def say(text):
	os.system('say "%s"'%text)

def close():
	wit.close()



app = ShelldonApp()
    
if os.path.isfile('shelldon.json'):
	prefs = json.load(open('shelldon.json'))
	app.title = prefs['currentDirectory']
	if app.title == None or app.title == "":
		app.title = "~"
	print app.title
	aliases = prefs['aliases']

wit.init()

def keydown_callback(event):
	print "KeyDown"+str(event)

def keyup_callback(event):
	print "KeyUp"+str(event)

#NSEvent.addGlobalMonitorForEventsMatchingMask_handler_(NSKeyDownMask, keydown_callback)
#NSEvent.addGlobalMonitorForEventsMatchingMask_handler_(NSKeyUpMask, keyup_callback)

if __name__ == "__main__":
	app.run()

